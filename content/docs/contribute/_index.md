---
weight: 5
title: Contribute
bookToc: false
bookFlatSection: true
---

<section id="contributors" class="bosun">
    <header class="bosun--header">
      <div class="bosun--mast">
        <h2>Reinvent the Social Web</h2>
        <h3>Want to Contribute?</h3>
      </div>
      <aside class="bosun--note">Help to build a decentralized social network protocol <img class="hermie-emoji" src="/images/hermie-emojis/hermie-hearts.png" alt="Hermie projects warm hearts your way"></aside>
    </header>
    <div class="bosun--content">
      <img class="bosun--icon" src="/images/ssbc.png" alt="Hermie hangs out inside a nice gray hexagon">
      <h4 class="bosun--subhead">SSBC</h4>
      <p class="bosun--detail">Secure Scuttlebutt Consortium</p>
      <a href="https://github.com/ssbc" class="button">Visit our GitHub</a>
    </div>
  </section>

&nbsp;

## Contributing

![Hermies watering](/images/gif/hermies-watering.gif)

This project is open commons that anyone can improve. We have a distributed network of decentralized designers, developers, storytellers and collaborators from across the globe. All kinds of contributions are welcome, such as but not limited to:


{{< columns >}} <!-- begin columns block -->
# Code
- bug reporting
- features
- tests
- peer review
- protocol design

<---> <!-- magic sparator, between columns -->

# Design
- project artwork
- user testing
- newbie first impressions
- real-world use cases

<---> <!-- magic sparator, between columns -->

# Facilitation
- pub server hosting
- community organizing (meetups)
- group collaboration
- conflict resolution

<---> <!-- magic sparator, between columns -->

# Storytelling
- blogs
- talks
- documentation
{{< /columns >}}
&nbsp;
___

&nbsp;
