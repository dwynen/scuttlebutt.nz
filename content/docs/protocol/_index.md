---
weight: 4
---

# Protocol

## The Scuttlebutt Protocol

Scuttlebutt is a protocol for building decentralized applications that works well offline and that no one person can control. Because there is no central server, Scuttlebutt clients connect to their peers to exchange information.

Scuttlebutt is a flexible protocol, capable of supporting many different types of applications. One of its first applications was as a social network, and it has also become one of the most compelling because the people who hang out there are not jerks.

For a comprehensive overview, see the [Scuttlebutt Protocol Guide](https://ssbc.github.io/scuttlebutt-protocol-guide/). You can also check out the developer documentation at [dev.scuttlebutt.nz](https://dev.scuttlebutt.nz/).

<p align="center">
<a href="https://ssbc.github.io/scuttlebutt-protocol-guide/">
<img class="invert" src="/images/guide/ssb-protocol.png">
</a>
</p>
